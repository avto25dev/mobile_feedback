/**
 * Created by webs on 22.04.16.
 */
'use strict';
import $ from 'jquery';
import tmpl from 'tmpl';
import loaders from 'loaders';
import tmp_widget from './templates/widget.html!text';
let el = $('#a25_mobile-feetback');
// загрузка стиля
let _pathCss = el.data('pathcss') || 'styles/';
let _fileCss = el.data('filecss') || 'main.dev.min.css';
let pathCss = currPath() + _pathCss;
loaders.loadCSS(pathCss + _fileCss, () => {
    el.attr('style', '');
});


let tplPhone = el.data('phone') || '';
let tplPhoneTitle = el.data('phoneTitle') || 'Позвонить';
let tplMailTitle = el.data('mailTitle') || 'Написать письмо';
var data = {
    phone: tplPhone,
    phoneTitle: tplPhoneTitle,
    mailTitle: tplMailTitle
};
let result = tmpl(tmp_widget, data);
el.html(result);

let el_mainMenuButton = $('.js-toush');
let el_mainMenuBlock = $('.js-menu-show');
let el_mainMenuItem = $('.js-menu__el');
let el_formBlock = $('.js-form-show');
let el_mainMenuItemMail = $('.js-mail_form-show');
let el_menu = '';

// разворот меню
$(document).on('click', el_mainMenuButton.selector, (e) => {
    el_mainMenuBlock.toggleClass('s-widget--show-menu', '');
    el_menu = $('#a25_mobile-feetback .s-widget--show-menu');

    if (el_menu.length === 1) {
        $(el_mainMenuItemMail.selector).on('click', (e) => {

            el_formBlock.removeClass('s-widget--hidden');

            $('.js-mobile_call--close').on('click', (e) => {
                el_formBlock.addClass('s-widget--hidden');
            });
        });
    } else {
        $(el_mainMenuItemMail.selector).off('click');
    }
});

let countItemMenu = el_mainMenuItem.length;
if (countItemMenu >= 0) countItemMenu--;
let counter = 0;

// смена изображения кнопки при дефолтном состоянии
setInterval(() => {
    if (el_menu.length === 0) {
        if (counter > countItemMenu) counter = 0;

        $(el_mainMenuItem).removeClass('s-show_ico');
        $(el_mainMenuItem[counter]).addClass('s-show_ico');
        counter++;
    }

}, 2000);

function currPath() {
    let scripts = document.getElementsByTagName('script');

    // с www и без
    let stepUndo = 3;
    if (/wwww./.test(scripts)) stepUndo = 4;
    let currPath = scripts[scripts.length - 1].src.split('/').slice(stepUndo).slice(0, -1).join('/');

    return `/${currPath}/`;
}

export default {};
