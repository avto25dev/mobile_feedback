# Кнопка звонка по телефону и отправики сообщения(email)
Проверки устройства или разрешения экрана(mediaquery) делается разработчиком

## установка 
В теле документа
```
    <div id="a25_mobile-feetback" data-phone="+7(495)227-74-83" style="display: none;"></div>
или
    <div id="a25_mobile-feetback" data-phone="+7(495)227-74-83" data-pathcss="styles/" data-filecss="main.min.css" style="display: none;"></div>
    …
    <!--в конце документа-->
     <script src="/bundle/blogApp/index.polyf.min.js"></script>
     <script src="/bundle/blogApp/index.min.js"></script>
```

## Параметры

* data-phone - телефон на который будет совершаться звонок(только для телефонов)

* data-pathcss - путь к стилям «кнопки». расчет идет от места залегания скрипта, по умолчанию без параметров путь styles/

пример: для 
```
<div id="a25_mobile-feetback" data-phone="+7(928)123-45-67" style="display: none;"></div>
```
```
styles/
|    |
|    main.dev.min.css
|    main.min.css
index.min.js
```

пример: для 
```
<div id="a25_mobile-feetback" data-phone="+7(928)123-45-67" data-pathcss="styles/" data-filecss="main.min.css" style="display: none;"></div>
```
```
main.dev.min.css
main.min.css
index.min.js
```

* data-filecss - указание какой css файл грузить